const sqlite3 = require("sqlite3").verbose();

// connect db
const db = new sqlite3.Database(
  "./database/transactions.db",
  sqlite3.OPEN_READWRITE,
  (err) => {
    if (err) return console.error(err.message);
  }
);

// GLOBAL
const runSQL = async (sql) => {
  return await new Promise((resolve, reject) => {
    db.all(sql, [], (err, rows) => {
      if (err) reject(err);
      resolve(rows);
    });
  });
};

exports.getLatestAmountByToken = async (token) => {
  let sql = `SELECT (dep - wit) as amount 
    FROM (
      (SELECT sum(amount) as dep FROM transactions WHERE token='${token}' AND transaction_type = 'DEPOSIT'),
      (SELECT sum(amount) as wit FROM transactions WHERE token='${token}' AND transaction_type = 'WITHDRAWAL')
    )
    `;

  return await runSQL(sql);
};

exports.getLatestAmountByDateAndToken = async (token, date) => {
  // date since epoch
  const millisec = 1000;
  const d = new Date(date);
  const seconds = d.getTime() / millisec;

  let sql = `SELECT ( COALESCE (dep, 0) - COALESCE (wit, 0) ) as amount
    FROM (
      (SELECT sum(amount) as dep FROM transactions WHERE token='${token}' AND transaction_type = 'DEPOSIT' AND timestamp <= ${seconds}),
      (SELECT sum(amount) as wit FROM transactions WHERE token='${token}' AND transaction_type = 'WITHDRAWAL' AND timestamp <= ${seconds})
    )
    `;

  return await runSQL(sql);
};

exports.getDistictToken = async () => {
  let sql = `SELECT DISTINCT token FROM transactions`;
  const tokens = await runSQL(sql);
  return tokens.map((a) => a.token);
};
