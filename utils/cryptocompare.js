exports.getUSDPriceByToken = async (token) => {
  const url = `https://min-api.cryptocompare.com/data/price?fsym=${token}&tsyms=USD`;
  const response = await fetch(url);
  return await response.json();
};
