const { extname } = require("path");

exports.migrate = (filePath) => {
  if (!filePath) {
    console.log("input file path");
    return;
  }

  if (extname(filePath).toLowerCase() === ".csv") {
    const sqlite3 = require("sqlite3").verbose();
    const db = new sqlite3.Database(
      "./database/transactions.db",
      sqlite3.OPEN_READWRITE,
      (err) => {
        if (err) return console.error(err.message);
      }
    );

    const csv = require("csv-parser");
    const fs = require("fs");

    // create table
    db.run(
      "CREATE TABLE IF NOT EXISTS transactions ( timestamp INTEGER, transaction_type TEXT, token TEXT, amount REAL, test TEXT);"
    );
    // db.exec( 'DROP TABLE transactions1;' );

    const SQLInsert =
      "INSERT INTO transactions ( timestamp, transaction_type, token, amount ) VALUES (?, ?, ?, ?)";

    fs.createReadStream(filePath)
      .pipe(csv({ separator: "," }))
      .on("data", (row) => {
        const insertRow = [
          row.timestamp,
          row.transaction_type,
          row.token,
          row.amount,
        ];
        db.run(SQLInsert, insertRow, function (err) {
          if (err) {
            return console.log(err.message);
          }
          // get the last insert id
          console.log(`A row has been inserted with rowid ${this.lastID}`);
        });
      })
      .on("end", () => {
        console.log("CSV file successfully processed");
        db.close();
      });
  }
};
