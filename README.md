
### CODE DESIGNS
##### CLI
> #### commander (https://www.npmjs.com/package/commander)
> - most used cli for nodejs

##### DATABASE
> #### sqlite (https://www.npmjs.com/package/sqlite3)
> - FS and FILE STREAM with 3m records of data will query super slow
> - it not require a server
> - it can aggregate big data
> - it can accomplish the needed task in the problem
> - SQL VS NOSQL = SQL because CSV is already structured (Standardized schema)

##### CODE STRUCTURE:
https://github.com/goldbergyoni/nodebestpractices#1-project-structure-practices

<br />
<br />

### STEPS TO RUN THE APP

#### 1. install the node module
```
npm i
```

<br />

#### 2. Migrate CSV file to transactions.db
> node .\cli.js migrate --fp "< FILE  PATH  OF  CSV >"
> !! Migrating using CLI takes around 30 mins to complete
```
node .\cli.js migrate --fp "C:\Users\Name\Downloads\transaction.csv"
```
#####  other migrating options
`2.1.` Import CSV using Portable **DB Browser for SQLite** (https://sqlitebrowser.org/dl/)

 1. Open the database then target *.\database\transactions.db*
 2. Delete existing Transaction Table
     In the Panel View > Tables (1) > transactions > ( *right click* then **Delete table** )
 3. Import CSV File
	 File > Import > Table from CSV file ... > Choose the CSV target
 4. Setup the CSV Settings 
    | Settings | Value|
    |--|--|
    | Table name | transactions |
    | Column names in first line | checked |
    | Field Separator | , |
    | Qoute character | " |
    | Encoding | UTF-8 |
    | Trim fields? | checked | 
 5. Click ok and wait for the import to finish
 6. in the menu near **x** button (upper right), click *X Close Database* then click Save
 7. Done

`2.2.` Download the Processed Database and change the transaction.db
- checkout to 'origin/migrated-database-file'
- or download it in the link
    https://gitlab.com/ejcajano/propineinterview/-/blob/migrated-database-file/transactions.db

  
<br />

#### 3.  Use the program CLI

Date values can be generated in the link (https://www.epoch101.com/) to get exact timestamp from the csv files.

##### COMMANDS
How to use commander.
```
node .\cli.js --help
```

<br />

##### Given no parameters, return the latest portfolio value per token in USD
> getPortfolio
```
node .\cli.js getPortfolio
```
<br />

##### Given a token, return the latest portfolio value for that token in USD
> getPortfolioByToken [token]
> node .\cli.js getPortfolioByToken --t [token]
> node .\cli.js getPortfolioByToken --token [token]

TOKENS
    - BTC
    - ETH
    - XRP

```
node .\cli.js getPortfolioByToken --t BTC
```

  <br />


##### Given a date, return the portfolio value per token in USD on that date
> getPortfolioByDate
> node .\cli.js getPortfolioByDate --d  [date]
> node .\cli.js getPortfolioByDate --date  [date]

Date must be supported by this 
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date

```
node .\cli.js getPortfolioByDate --d "Fri, 25 Oct 2019 00:08:52 GMT"
```

<br />

#####  Given a date and a token, return the portfolio value of that token in USD on that date
> getPortfolioByTokenAndDate 
> node .\cli.js getPortfolioByTokenAndDate --t [token] --d [date]
> node .\cli.js getPortfolioByTokenAndDate --token [token] --date [date]
```
node .\cli.js getPortfolioByTokenAndDate --t BTC --d "Fri, 25 Oct 2019 00:08:52 GMT"
```