const { program } = require("commander");
const {
  getPortfolio,
  getPortfolioByToken,
  getPortfolioByDate,
  getPortfolioByTokenDate
} = require("./controllers/portfolio");

const { migrate } = require("./database/migrate")

program
  .command("migrate")
  .option("--fp --filePath <value>", "file path input")
  .description(
    "Migrate CSV files to DB"
  )
  .action((input) => migrate(input.filePath));

program
  .command("getPortfolio")
  .description(
    "Given no parameters, return the latest portfolio value per token in USD"
  )
  .action(getPortfolio);

program
  .command("getPortfolioByToken")
  .description(
    "Given a token, return the latest portfolio value for that token in USD"
  )
  .option("--t --token <value>", "token input")
  .action((input) => getPortfolioByToken(input.token));


program
  .command("getPortfolioByDate")
  .description(
    "Given a date, return the portfolio value per token in USD on that date"
  )
  .option("--d --date <value>", "date input")
  .action((input) => getPortfolioByDate(input.date));

program
  .command("getPortfolioByTokenAndDate")
  .description(
    "Given a date and a token, return the portfolio value of that token in USD on that date"
  )
  .option("--t --token <value>", "token input")
  .option("--d --date <value>", "date input")
  .action((input) => getPortfolioByTokenDate(input.token, input.date));



program.parse(process.argv);
