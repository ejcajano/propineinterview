const {
  getLatestAmountByToken,
  getLatestAmountByDateAndToken,
  getDistictToken,
} = require("../utils/sqlFunctions");
const { getUSDPriceByToken } = require("../utils/cryptocompare");

const answerModel = (amountByToken, tokenPrice, token) => {
  return {
    token,
    value: amountByToken[0].amount,
    USD: amountByToken[0].amount * tokenPrice.USD + " USD",
  };
};

exports.getPortfolio = async () => {
  let result = [];
  const tokens = await getDistictToken();

  for (const token of tokens) {
    let amountByToken = await getLatestAmountByToken(token);
    let tokenPrice = await getUSDPriceByToken(token);
    result.push(answerModel(amountByToken, tokenPrice, token));
  }

  // SHOW ANSWER
  console.log(result);
};

exports.getPortfolioByToken = async (token) => {
  let result;
  const tokens = await getDistictToken();

  if (tokens.includes(token)) {
    let amountByToken = await getLatestAmountByToken(token);
    let tokenPrice = await getUSDPriceByToken(token);
    result = answerModel(amountByToken, tokenPrice, token);
  } else {
    result = `tokens available: ${tokens}`;
  }

  console.log(result);
};

exports.getPortfolioByDate = async (date) => {
  let result = [];
  let parsedDate = Date.parse(date);

  if (isNaN(date) && !isNaN(parsedDate)) {
    const tokens = await getDistictToken();

    for (const token of tokens) {
      let amountByToken = await getLatestAmountByDateAndToken(token, date);
      let tokenPrice = await getUSDPriceByToken(token);

      result.push(answerModel(amountByToken, tokenPrice, token));
    }
  } else {
    result = "Input not valid Date";
  }

  console.log(result);
};

exports.getPortfolioByTokenDate = async (token, date) => {
  let parsedDate = Date.parse(date);
  const tokens = await getDistictToken();

  if (!tokens.includes(token)) {
    result = `tokens available: ${tokens}`;
  } else if (isNaN(date) && isNaN(parsedDate)) {
    result = "Input not valid Date";
  } else {
    let amountByToken = await getLatestAmountByDateAndToken(token, date);
    let tokenPrice = await getUSDPriceByToken(token);
    result = answerModel(amountByToken, tokenPrice, token);
  }

  console.log(result);
};
